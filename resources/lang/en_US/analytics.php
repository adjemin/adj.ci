<?php

/*
 * UrlHum (https://urlhum.com)
 *
 * @link      https://github.com/urlhum/UrlHum
 * @copyright Copyright (c) 2019 Christian la Forgia
 * @license   https://github.com/urlhum/UrlHum/blob/master/LICENSE.md (MIT License)
 */

return [
  'referer' => [
    'referers' => 'Référents',
    'referer' => 'Référent',
    'list' => [
      'results' => 'Affichage de :firstItem - :lastItem sur :num au total',
    ],
    'na' => 'Aucun référents diponible. Veuillez partager le lien pour en obtenir!',
    'best' => 'Meilleur référents',
    'direct_unknown' => 'Direct / Inconnu',
  ],
  'click' => [
    'click' => 'Cliquer',
    'reals' => 'Cliques réels',
    'clicks' => 'Cliques',
    'today' => 'Cliques d\'aujourd\'hui',
    'real' => 'Clique réel',
    'latest' => 'Dernier Clique',
    'latests' => 'Derniers cliques',
    'na' => 'Désolé, nous n\'avons pas assez de données pour afficher les cliques.',
  ],
  'show' => [
    'title' => 'Statistiques pour l\'url',
  ],
  'country' => [
    'real' => 'Cliques réels par pays',
    'na' => 'Désolé, nous n\'avons pas assez de données pour afficher les cliques. Partagez ce lien!',
    'views' => 'Cliques par pays',
  ],
  'url' => [
    'created' => 'Liens courts créés',
  ],
  'analytics' => 'Statistiques',
  'time' => 'Temps',
];
