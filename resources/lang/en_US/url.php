<?php

/*
 * UrlHum (https://urlhum.com)
 *
 * @link      https://github.com/urlhum/UrlHum
 * @copyright Copyright (c) 2019 Christian la Forgia
 * @license   https://github.com/urlhum/UrlHum/blob/master/LICENSE.md (MIT License)
 */

return [
  'list' => 'List des URls',
  'edit' => [
    'edit' => 'Modifier l\'URL',
    'short' => 'Modifier le lien court',
  ],
  'created' => 'URL créée :date',
  'url' => 'URL',
  'my' => [
    'my' => 'Mes liens courts',
    'list' => 'Affichage de :firstItem - :lastItem lien(s) court(s) sur :num au total',
  ],
  'public' => 'Les dernières URLs publiques',
  'short' => 'Lien court',
  'new' => 'Nouvel URL',
  'long' => 'Lien long',
  'delete' => [
    'confirm' => 'Êtes vous sûre de vouloir supprimer ce lien court? Toutes les données tattachées seront perdu.',
    'delete' => 'Supprimer l\'URL',
  ],
  'listres' => 'Affichage de :firstItem - :lastItem lien(s) court(s) sur :num au total',
  'updated' => 'URL modifiée le :date',
  'destination' => 'Destination',
  'by' => 'Créé par',
  'stats' => 'Afficher les statistiques',
  'options' => [
    'hide' => 'Masquer pour les pages publiques',
    'private_stats' => 'Rendre l\'affichage des statistiques privé pour ce lien',
    'destination' => 'Utl de destination',
    'options' => 'Options',
    'custom' => 'Lien court personnalisé',
  ],
  'id' => 'ID',
  'createdStr' => 'Créé',
  'action' => 'Action',
  'existing_long' => 'Ce long lien existe déjà',
  'success' => 'Lien court créé avec succès!',
  'existing_custom' => 'Le lien court :url existe déjà. Veuillez en choisir un autre',
  'new_long_text' => 'Créer un nouveau lien court',
  'latest' => 'Dernières URLs',
  'all' => 'Tout afficher',
  'urls' => 'URLs',
  'qrcode' => 'QR Code',
  'qrcode_modal' => 'QR Code',
  'qrcode_download' => 'Télécharger',
];
