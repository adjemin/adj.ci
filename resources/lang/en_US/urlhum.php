<?php

/*
 * UrlHum (https://urlhum.com)
 *
 * @link      https://github.com/urlhum/UrlHum
 * @copyright Copyright (c) 2019 Christian la Forgia
 * @license   https://github.com/urlhum/UrlHum/blob/master/LICENSE.md (MIT License)
 */

return [
  'privacypolicy' => 'Politique de confidentialité',
  'and' => 'et',
  'termsofuse' => 'Conditions d\'utilisation',
  'dashboard' => 'Tableau de bord',
  'design_by' => 'Powered by',
  'users' => 'Utilisateurs',
  'welcome' => 'Bienvenue!',
  'accessFeatures' => 'Inscrivez-vous ou connectez-vous pour bénéficier de toutes les fonctionnalités',
  'search' => 'Recherche',
  'account' => 'Compte',
  'logout' => 'Déconnexion',
  'register' => 'Inscription',
  'login' => 'Connexion',
  'home' => 'Acceuil',
  'save' => 'Enregistrer',
  'error' => 'Erreur',
  'send' => 'Envoyer',
  'close' => 'Fermer',
  'edit' => 'Modifier',
  'user' => 'Utilisateur',
];
