<?php

/*
 * UrlHum (https://urlhum.com)
 *
 * @link      https://github.com/urlhum/UrlHum
 * @copyright Copyright (c) 2019 Christian la Forgia
 * @license   https://github.com/urlhum/UrlHum/blob/master/LICENSE.md (MIT License)
 */

return [
  'update_success' => 'Compte modifié avec succès!',
  'password' => [
    'success' => 'Mot de passe modifié avec succès!',
    'current' => 'Mot de passe actuel',
    'not_match' => 'Le mot de passe ne correspond pas à votre mot de passe actuel',
    'password' => 'Mot de passe',
    'forgot' => 'Mot de passe oublié?',
    'reset' => 'Reinitialiser le mot de passe',
    'confirm' => 'Confirmer le nouveau mot de passe',
    'new' => 'Nouveau mot de passe',
    'change' => 'Changer le mot de passe',
    'old' => 'Ancien mot de passe',
    'send' => 'Envoyer un lien de reinitialisation',
  ],
  'user_created' => 'Utilisateur créé avec succès',
  'user_updated' => 'Utilisateur modifié avec succès',
  'user_deleted' => 'Utilisateur supprimé avec succès',
  'sign_in' => 'Entrez vos identifiants',
  'email' => 'Adresse mail',
  'remember' => 'Se souvenir de moi',
  'login' => 'Se connecter',
  'new' => 'Créer un compte',
  'sign_up' => 'S\'inscrire sur :site',
  'name' => 'Nom',
  'agreement' => 'J\'approuve les',
  'agreement_error' => 'Pour continuer, veuillez approuver nos termes de confidentialité.',
  'create' => 'Créer le compte',
  'verify' => 'Vérifier votre adresse mail.',
  'verify_sent' => 'Un lien de vérification a été envoyé à votre adresse mail.',
  'verify_proceed' => 'Pour continuer, veuillez consulter votre adresse mail pour la vérification.',
  'verify_resend' => 'Si vous n\'avez pas reçu d\'email',
  'verify_resend_2' => 'cliquer pour reçevoir un nouveau line',
  'verified_title' => 'Compte vérifié avec succès!',
  'verified' => 'Bravo! Votre adresse mail a été vérifiée avec succès. Vous pouvez accéder à toutes les fonctionnalités de la plateforme.',
  'edit' => 'Modifier le compte',
  'information' => 'Informations de l\'utilisateur',
  'user' => 'Utilisateur',
  'management' => 'Gestion des utilisateurs',
  'back_list' => 'Retour à la liste',
  'users' => 'Utilisateurs',
  'add' => 'Ajouter un utilisateur',
  'creation_date' => 'Date de création',
  'delete' => [
    'confirm' => 'Voulez-vous vraiment supprimer cet utilisateur?',
    'delete' => 'Supprimer l\'utilisateur',
  ],
];
