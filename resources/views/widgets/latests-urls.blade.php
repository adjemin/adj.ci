@include('styleCopy')
<style>
  .module {
  width: 350px;
  margin: 0 0 0 0;
  overflow: hidden;
  text-overflow: ellipsis;
}
</style>
<div class="
@if ($anonymous OR !isAdmin() OR setting('disable_referers'))
col-12
@else
col-xl-7
@endif
        mb-5 mb-xl-0">
    <div class="card shadow">
        <div class="card-header border-0">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="mb-0">{{ __('url.latest') }}</h3>
                </div>
                <div class="col text-right">
                    <a href="{{ route('url.public') }}" class="btn btn-sm btn-success">{{ __('url.all') }}</a>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <!-- Projects table -->
            <table class="table align-items-center table-flush">
                <thead class="thead-light">
                <tr>
                    <th scope="col">{{ __('url.short') }}</th>
                    <th scope="col">{{ __('url.long') }}</th>
                    {{-- <th scope="col">{{ __('analytics.click.clicks') }}</th> --}}
                    {{-- @if (isAdmin())
                    <th scope="col">{{ __('url.action') }}</th>
                    @endif --}}
                    <th>stats</th>
                </tr>
                </thead>
                <tbody>
                @if($publicUrls->count())
                @foreach ($publicUrls as $key => $publicUrl)
                    <tr>
                    <td scope="row">
                     <div class="row">
                         <div class="col-md-6" style="margin-bottom:10px;">
                        {{-- <span id="copy_link{{$loop->iteration}}">https://adj.ci/{{$publicUrl->short_url}}</span><br> --}}
                         <span id="copy_result_input{{$loop->iteration}}" class="text-success"></span>

                            <input type="text" style="border:none;width=100px;" value="https://adj.ci/{{$publicUrl->short_url}}" id="input{{$loop->iteration}}" readonly>
                         </div>
                         <div class="col-md-1"></div>
                         <div class="col-md-6">
                            <div class="tooltip">
                                <button style="padding:5px;" class="btn btn-sm btn-info" onclick="myFunction('input{{$loop->iteration}}')" onmouseout="outFunc()">
                                    <span class="tooltiptext" id="myTooltip{{$loop->iteration}}">Copier lien court</span>
                                    <i class="fa fa-copy"></i>
                                    <span></span>
                                </button>
                            </div>
                            @auth()
                            <div class="tooltip">
                                
                                  
                                <a style="padding:5px;" href="/url/{{$publicUrl->short_url}}" class="btn btn-sm btn-success">
                                        <span class="tooltiptext" id="">Modifier lien court</span>
                                        <i class="fa fa-edit"></i>
                                        <span></span>
                                    </a>
                             
                            </div>
                            <div class="tooltip">
                                <span class="tooltiptext" id="" style="font-size:12px;"><b>Suprimer lien court</b></span>
                                <form action="/url/{{$publicUrl->short_url}}" method="POST" id="xxxx">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input style="padding-left:15px;padding-right:15px;padding-bottom:5px;padding-top:5px;" type="submit" class="btn btn-sm btn-danger" value="X"
                                           onclick="confirmDelete()">
                                </form>
                               </span>
                            </div>
                            @endauth()
                         </div>
                         
                         {{-- <div class="col-md-1"></div>
                         <div class="col-md-2">
                            <div class="tooltip">
                                <button class="btn btn-sm btn-danger" onclick="myFunction('input{{$loop->iteration}}')" onmouseout="outFunc()">
                                    <span class="tooltiptext" id="myTooltip{{$loop->iteration}}">Copier lien court</span>
                                    Copier
                                </button>
                            </div>
                         </div> --}}
                     </div>
                        </td>
                        <td>
                            <div class="module overflow">
                                <a href="{{$publicUrl->long_url}}" target="_blank">{{$publicUrl->long_url }}</a>
                              </div>
                        </td>
                        {{-- <td>
                            {{$publicUrl->clicks}}
                        </td> --}}
                        <td>
                            {{$publicUrl->clicks}} {{ __('analytics.click.clicks') }} &nbsp;

                            <a title="Voir les statistiques"  href="/{{$publicUrl->short_url}}+" class="mr-2">
                                <i class="fa fa-chart-bar fa-2x"></i>
                            </a>
                        </td>
                        {{-- <td>
                          
                        @if (isAdmin())
                        {{$publicUrl->clicks}} {{ __('analytics.click.clicks') }} &nbsp;

                        <a title="Voir les statistiques"  href="/{{$publicUrl->short_url}}+" class="mr-2">
                            <i class="fa fa-chart-bar fa-2x"></i>
                        </a>
                            <a href="/url/{{$publicUrl->short_url}}">
                                <button type="button" class="btn btn-success btn-sm btn-url-edit"><i
                                            class="fa fa-pencil-alt" alt="Edit"> </i> {{ __('urlhum.edit') }}
                                </button>
                            </a>
                        @endif
                        </td> --}}
                    </tr>
                    @include('copyScript') 
                @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

@push('js')

{{-- <script src="/js/app.js"></script>

<script>
    $.noConflict();
    $("#xxxx").submit(function (e) {
        if (confirm("{{ __('url.delete.confirm') }}")) {
            $("#xxxx")[0].submit();
        }
        e.preventDefault();
    });
</script> --}}

@endpush