@extends('layouts.app',  ['title' => trans('url.public')])
@include('styleCopy')
<style>
    .module {
    width: 350px;
    margin: 0 0 0 0;
    overflow: hidden;
    text-overflow: ellipsis;
  }
  </style>
@section('content')
    <div class="header bg-gradient-primary mb-3 pt-6 	d-none d-lg-block d-md-block pt-md-7"></div>
    <div class="container-fluid">
        <div class="header-body">
            <div class="card">
                <div class="row">
                    <div class="col">
                        <div class="card shadow">
                            <div class="card-header border-0">
                                <div class="row align-items-center">
                                    <div class="col-8">
                                        <h3 class="mb-0">{{ __('url.public') }}</h3>
                                    </div>
                                    <div class="col-4 text-right">
                                        <a href="{{ route('home') }}" class="btn btn-sm btn-primary">{{ __('url.new') }}</a>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table align-items-center table-flush">
                                    <thead class="thead-light">
                                    <tr>
                                        <th scope="col">{{ __('url.short')  }}</th>
                                        <th scope="col">{{ __('url.long') }}</th>
                                        <th scope="col">{{ __('analytics.analytics') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($urls as $url)
                                        <tr>
                                            <td scope="row">
                                                <div class="row">
                                                    <div class="col-md-9" style="margin-bottom:5px;">
                                                   {{-- <span id="copy_link{{$loop->iteration}}">https://adj.ci/{{$url->short_url}}</span><br> --}}
                                                    <span id="copy_result_input{{$loop->iteration}}" class="text-success"></span>
                           
                                                       <input type="text" style="border:none;width=100px;" value="https://adj.ci/{{$url->short_url}}" id="input{{$loop->iteration}}" readonly>
                                                    </div>
                                                    <div class="col-md-1"></div>
                                                    <div class="col-md-4">
                                                       <div class="tooltip">
                                                           <button style="padding:8px;" class="btn btn-sm btn-info" onclick="myFunction('input{{$loop->iteration}}')" onmouseout="outFunc()">
                                        
                                                               <span class="tooltiptext" id="myTooltip{{$loop->iteration}}">Copier lien court</span>
                                                               <i class="fa fa-copy"></i>
                                                               <span></span>
                                                           </button>
                                                       </div> <br>
                                                    </div>
                                                    @auth()
                                                    <div class="col-md-4">
                                                        <div class="tooltip">
                                                            <form action="/url/{{$url->short_url}}" method="post">
                                                                @csrf
                                                                <a style="padding:8px;" href="/url/{{$url->short_url}}" class="btn btn-sm btn-success">
                                                                    <span class="tooltiptext" id="">Modifier lien court</span>
                                                                    <i class="fa fa-edit"></i>
                                                                    <span></span>
                                                                </a>
                                                            </form>
                                                        </div> 
                                                        <br>
                                                     </div>
                                                     <div class="col-md-4">
                                                        <div class="tooltip">
                                                            <span class="tooltiptext" id="" style="font-size:12px;"><b>Suprimer lien court</b></span>
                                                            <form action="/url/{{$url->short_url}}" method="POST" id="deleteUrl">
                                                                @csrf
                                                                <input type="hidden" name="_method" value="DELETE">
                                                                <input style="padding-left:15px;padding-right:15px;padding-bottom:5px;padding-top:5px;" type="submit" class="btn btn-sm btn-danger" value="X"
                                                                       onclick="confirmDelete()">
                                                            </form>
                                                           </span>
                                                        </div>
                                                        <br>
                                                     </div>
                                                     @endauth()
                                                </div>
                                            </td>
                                           
                                            <td>
                                            <div class="module overflow">
                                                <a target="_blank" href="{{$url->long_url}}">{{$url->long_url}}</a>
                                            </div>
                                            </td>
                                            <td>
                                                {{$url->clicks}} {{ __('analytics.click.clicks') }} &nbsp;
                                                <a title="Voir Statistiques" href="/{{$url->short_url}}+" class="mr-2">
                                                    <i class="fa fa-chart-bar fa-2x"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        @include('copyScript') 
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="card-footer py-4">
                                <nav class="d-flex justify-content-between" aria-label="...">
                                    <p class="pull-left">
                                        {{ __('url.listres', [
                                            'firstItem' => $urls->firstItem(),
                                            'lastItem' => $urls->lastItem(),
                                            'num' => $urls->total()
                                        ]) }}
                                    </p>
                                    <div class="pull-right">
                                        {{ $urls->links() }}
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>

@endsection
@push('js')
    <script src="/js/app.js"></script>
@endpush