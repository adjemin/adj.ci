<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale('fr')) }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> {{ $title ?? ''}} - {{ setting('website_name') }}</title>
    <!-- Favicon -->
    <link href="{{ setting('website_favicon') }}" rel="icon" type="image/png">
   
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700" rel="stylesheet">
    <!-- Icons -->
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-160867123-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-160867123-1');
    </script>
    <style>
        
    </style>
</head>
<body class="{{ $class ?? '' }}">
    @include('sweetalert::alert')

@auth()
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
    @include('layouts.navbars.sidebar')
@endauth

<div class="main-content">
    @include('layouts.navbars.navbar')
    @yield('content')
</div>





@stack('js')
{!! setting('custom_html') !!}
{{-- @include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"]) --}}
{{-- <script src="{{asset('js/chart.js/Chart.min.js')}}"></script>
<script src="{{asset('js/chart.js/Chart.bundle.min.js')}}"></script> --}}
{{-- <script src="{{asset('js/chart.js/dist/Chart.extension.js')}}"></script> --}}
</body>
</html>